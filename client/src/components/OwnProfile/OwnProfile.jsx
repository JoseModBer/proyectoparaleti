// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

import './OwnProfile.css';
import { useToken } from '../../TokenContext';
import { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';
import { NavLink } from 'react-router-dom';

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN OwnProfile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const OwnProfile = () => {
  const [token, setToken] = useToken();
  const [userData, setUserData] = useState();
  const [userData2, setUserData2] = useState(null);
  const navigate = useNavigate();
  const [loading, setLoading] = useState();

  // Si estamos logueados redireccionamos a la página principal.
  if (!token) return <Navigate to="/" />;

  // Usamos un useEffect para conseguir los datos de un usuario en concreto (usuario logueado):
  useEffect(() => {
    const fetchUser = async () => {
      try {
        const res = await fetch('http://127.0.0.1:4000/users', {
          headers: {
            Authorization: token,
          },
        });
        const userFromServer = await res.json();

        if (userFromServer.status === 'error') {
          alert(userFromServer.message);
        } else {
          setUserData({ ...userFromServer.data });
        }
      } catch (err) {
        console.error(err);
      }
    };
    fetchUser();
  }, []);

  // Función fetchPhotosData para obtener las fotos del usuario.
  const fetchPhotosData = async () => {
    setLoading(true);
    try {
      const res = await fetch(
        `http://127.0.0.1:4000/users/${userData.user.id}`
      );
      const body = await res.json();

      if (body.status === 'error') {
        alert(body.message);
      } else {
        setUserData2(body.data);
      }
    } catch (err) {
      console.error(err);
    }
    setLoading(false);
  };

  // Función fetchDeleteUser para borrar el usuario sus entradas y demás...
  const fetchDeleteUser = async () => {
    setLoading(true);
    if (
      confirm(
        '¿Estás seguro de que deseas borrar tu usuario? (Se borrarán todas tus entradas, fotos, comentarios y likes)'
      )
    ) {
      try {
        const res = await fetch(`http://127.0.0.1:4000/users`, {
          method: 'delete',
          headers: {
            Authorization: token,
          },
        });

        alert('Se ha borrado tu usuario correctamente.');
      } catch (err) {
        console.error(err);
      }
    }
    setToken(null);
    setLoading(false);
  };

  return (
    <main>
      <div className="div-ownprofile-container">
        {' '}
        {userData && (
          <>
            <h2>Datos de perfil de tu perfil</h2>
            <p className="p-ownprofile-sectiontitle">Nombre de usuario:</p>
            <p>@{userData.user.name}</p>
            <p className="p-ownprofile-sectiontitle">Email:</p>
            <p>{userData.user.email}</p>
            <p className="p-ownprofile-sectiontitle">
              Fecha de creación del perfil:
            </p>
            <time dateTime={userData.user.created_at}>
              {new Date(userData.user.created_at).toLocaleDateString('es-ES', {
                hour: '2-digit',
                minute: '2-digit',
                day: '2-digit',
                month: '2-digit',
                year: '2-digit',
              })}
            </time>
          </>
        )}
      </div>

      <div className="div-ownprofilebutton-container">
        <button
          disabled={loading}
          onClick={() => {
            fetchPhotosData();
          }}
        >
          Mostrar mis imágenes
        </button>
      </div>

      {userData2 && (
        <ul className="ul-profile-photocontainer">
          {userData2 &&
            userData2.user.entries.map((entry) => {
              return (
                <>
                  {entry.photos.map((photo) => {
                    return (
                      <li key={entry.photos.id}>
                        <img
                          src={`http://localhost:4000/${photo.photo_name}`}
                          alt="foto"
                        ></img>
                      </li>
                    );
                  })}
                </>
              );
            })}
        </ul>
      )}

      <div className="div-ownprofilebutton-container">
        <NavLink to="/">
          <button
            disabled={loading}
            onClick={() => {
              fetchDeleteUser();
            }}
          >
            Borrar usuario
          </button>
        </NavLink>
      </div>
    </main>
  );
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS OwnProfile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

export { OwnProfile };
