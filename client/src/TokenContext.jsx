// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%

import { createContext, useState, useContext } from 'react';

const TokenContext = createContext(null);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN TokenProvider %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const TokenProvider = ({ children }) => {
  const [token, setToken] = useState(localStorage.getItem('token'));

  const setTokenInLocalStorage = (newToken) => {
    if (!newToken) {
      localStorage.removeItem('token');
    } else {
      localStorage.setItem('token', newToken);
    }

    setToken(newToken);
  };

  return (
    <TokenContext.Provider value={[token, setTokenInLocalStorage]}>
      {children}
    </TokenContext.Provider>
  );
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN useToken %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const useToken = () => {
  return useContext(TokenContext);
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS TokenProvider y useToken %%%%%%%%%%%%%%

export { TokenProvider, useToken };
